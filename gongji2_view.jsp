<!- 글 하나 조회 ->

<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<%@ page contentType = "text/html; charset=utf-8"%>
<%@ page import="java.sql.*,javax.sql.*, java.io.*"%>
<html>
<title>게시글 조회</title>
<head>
<!--에러 잡는 try-catch문-->
<%!      
   public static int extractErrorCode (SQLException sqlException) {
      int errorCode = sqlException.getErrorCode();
      SQLException nested = sqlException.getNextException();
      while(errorCode==0&&nested!=null){
         errorCode=nested.getErrorCode();
         nested=nested.getNextException();
      }
      return errorCode;
   }
%>

<style type = "text/css">
  table {
	margin:auto;
    text-align: center;

    border: 5px 
  
  }
h2 {
   text-align:center;
}   
</style>
</head>

<%
try{
//MySQL드라이버 로딩: class클래스의 forName()메소드를 이용하여 interface driver 로드
	Class.forName("com.mysql.jdbc.Driver");
//MySQL Connection 객체 생성
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/seradatabase", "root","aeae");
//sql쿼리 생성/실행을 위한 statement객체 생성
	Statement stmt = conn.createStatement();
	Statement stmt2 = conn.createStatement();

//select sql쿼리 실행 결과물을 담는 ResultSet rset 생성
	//get방식이므로 파라메터를 String으로 받아옴
	String ids = request.getParameter("id");
	Integer id = 0;

   if(ids == null || ids.trim().equals("")){
	ids = "1";
   }
   id = Integer.parseInt(ids);
	ResultSet rset = stmt.executeQuery("select * from gongji2 where id = "+id+";");
	rset.next();
	String title = rset.getString(2); //제목
	Date today = rset.getDate(3); //날짜
	String content = rset.getString(4); //내용
	Integer rootid = rset.getInt(5); //글ID
	Integer relevel = rset.getInt(6); //댓글레벨
	Integer recnt = rset.getInt(7); //댓글내 순서
	Integer viewcnt = rset.getInt(8); //조회수
	// 매 조회시, 조회수 업데이트
	viewcnt++;
	stmt2.execute("update gongji2 set viewcnt="+viewcnt+" where id ="+id+";");
%>
<br>
<h2> <%=id%>번 게시물 </h2>
<br>
<table border = 1 cellpadding=5>
<!--form의 post방식으로 input name/value을 파라메터로 전달하지만
input type 이 hidden이어서 input칸 보이지않음 -->
	<tr>
	<td width=120>번호 </td>
	<td width=480 colspan=3 align=left><input type='hidden' name="id" value=<%=id%>><%=id%></td>
	</tr>
	<tr>
	<td width=120>제목 </td>
	<td width=480 colspan=3 align=left><input type='hidden' name="title" value=<%=title%>><%=title%></td>
	</tr>
	<tr>
	<td width=120>일자 </td>
	<td width=480 colspan=3 align=left><input type='hidden' name="today" value=<%=today%>><%=today%></td>
	</tr>
	<tr>
	<td width=120>조회수 </td>
	<td width=480 colspan=3 align=left><input type='hidden' name="viewcnt" value=<%=viewcnt%>><%=viewcnt%></td>
	</tr>
	<tr>
	<td width=120>내용 </td>
	<td width=480 colspan=3 align=left><input type='hidden' name="content" value=<%=content%>><%=content%></td>
	</tr>
	<tr>
	<td width=120>원글 </td>
	<td width=480 colspan=3 align=left><input type='hidden' name="rootid" value=<%=rootid%>><%=rootid%></td>
	</tr>
	<tr>
	<td width=120>댓글수준 </td>
	<td width=180 align=left><input type='hidden' name="relevel" value=<%=relevel%>><%=relevel%></td>
	<td width=120 align=left>댓글내 순서<input type='hidden' name ='recnt' value=<%=recnt%>></td>
	<td width=180 align=left><%=recnt%></td>
	</tr>
</table>
<br>
<table>
	<tr>
	<td>
	<input type=button onClick=location.href='gongji2_list.jsp' value = '목록'></input></td>
	<td>
	<input type=button onClick=location.href='gongji2_update.jsp?key=<%=id%>' value = '수정'> 
	</input></td>
	<td>
	<input type=button onClick=location.href='gongji2_delete.jsp?key=<%=id%>' value = '삭제'> 
	</input></td>
	<td>
	<input type=button onClick=location.href='gongji2_reinsert.jsp?key=<%=id%>' value = '댓글'> 
	</input></td>
	</tr>
</table>
<%
	rset.close();
	stmt.close();
	stmt2.close();
	conn.close();
} catch(SQLException e) {
      if(extractErrorCode(e)==1062){
            out.println(extractErrorCode(e)+" 에러가 발생했습니다.<br>");
            out.println("데이터가 중복됩니다.");
         }else if(extractErrorCode(e)==1146){
            out.println(extractErrorCode(e)+" 에러가 발생했습니다.<br>");
            out.println("테이블이 존재하지 않습니다.");
         }else{
            out.println("[기타] <br>");
            out.println(extractErrorCode(e)+" 에러가 발생했습니다.");
            out.println("입력값이 없습니다.");
         }
}
	
%>
</table>
</html>