<!- 글 수정 ->

<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<%@ page contentType = "text/html; charset=utf-8"%>
<%@ page import="java.sql.*,javax.sql.*, java.io.*"%>
<html>
<title>게시글 수정</title>
<head>
<%
 java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyy-MM-dd");
 String today = formatter.format(new java.util.Date());

 //out.println(today);

%>
<!--에러 잡는 try-catch문-->
<%!      
   public static int extractErrorCode (SQLException sqlException) {
      int errorCode = sqlException.getErrorCode();
      SQLException nested = sqlException.getNextException();
      while(errorCode==0&&nested!=null){
         errorCode=nested.getErrorCode();
         nested=nested.getNextException();
      }
      return errorCode;
   }
%>
<style type = "text/css">
  table {
	margin:auto;
    text-align: center;
	
    border: 5px 
  
  }
h2 {
   text-align:center;
}   
</style>
<script type="text/javascript">
<!--뒤로가기 메서드-->
function goBack(){
window.history.back();
}
</script>
</head>

<%
//try{
//MySQL드라이버 로딩: class클래스의 forName()메소드를 이용하여 interface driver 로드
	Class.forName("com.mysql.jdbc.Driver");
//MySQL Connection 객체 생성
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/seradatabase", "root","aeae");
//sql쿼리 생성/실행을 위한 statement객체 생성
	Statement stmt = conn.createStatement();
//post 방식으로 파라메터 가져올 때 한글 사용 코드
	request.setCharacterEncoding("UTF-8");
	String id = request.getParameter("key");
//select sql쿼리 실행 결과물을 담는 ResultSet rset 생성
//id에 해당하는 전체 칼럼 데이터 보기
ResultSet rset = stmt.executeQuery("select * from gongji2 where id = "+id+";");
	rset.next();
	String title = rset.getString(2);
	String content = rset.getString(4);
	Integer rootid = rset.getInt(5);
	Integer relevel = rset.getInt(6);
	Integer recnt = rset.getInt(7);
	Integer viewcnt = rset.getInt(8);
	
%>
<br>
<h2>게시글 수정</h2>
<table border = 1  cellpadding=5 colspan=5>
<form method=post action='gongji2_write.jsp?key=update&id=<%=id%>'>
	<tr>
	<td width=150>번호 </td>
	<td width=620 colspan=3 align=left><%=id%></td>
	</tr>
	<tr>
	<td width=150>제목 </td>
	<td width=620 colspan=3 align=left><input type=text required name="title" value=<%=title%>></input></td>
	</tr>
	<tr>
	<td width=150>일자 </td>
	<td width=620 colspan=3 align=left><%=today%></td>
	</tr>
	<tr>
	<td width=150>내용 </td>
	<td width=620 colspan=3 align=left height=200>
	<textarea required style="resize:none" name ="content" cols=100 rows=15><%=content%></textarea></td>
	</tr>
	<tr>
	<td width=150>원글 </td>
	<td width=620 colspan=3 align=left><input type='hidden' name="rootid" value=<%=rootid%>><%=rootid%></td>
	</tr>
	<tr>
	<td width=150>댓글수준 </td>
	<td width=235 align=left><input type='hidden' name="relevel" value=<%=relevel%>><%=relevel%>
	<td width=150 align=left> 댓글내 순서<input type='hidden' name ='recount' value=<%=recnt%>></td>
	<td width=235 align=left><%=recnt%></td>
</table>
<br>
<table>
	<tr>
	<td><input type=button onClick='goBack();' value = '취소'> </input></td>
	<td>
	<input type=submit value = '등록'></input></td>
</form>
	<td><input type=button onClick=location.href='gongji2_delete.jsp?id=<%=id%>'  value = '삭제'></input></td>
	</tr>
</table>
<%
	rset.close();
	stmt.close();
	conn.close();
// } catch(SQLException e) {
      // if(extractErrorCode(e)==1062){
            // out.println(extractErrorCode(e)+" 에러가 발생했습니다.<br>");
            // out.println("데이터가 중복됩니다.");
         // }else if(extractErrorCode(e)==1146){
            // out.println(extractErrorCode(e)+" 에러가 발생했습니다.<br>");
            // out.println("테이블이 존재하지 않습니다.");
         // }else{
            // out.println("[기타] <br>");
            // out.println(extractErrorCode(e)+" 에러가 발생했습니다.");
            // out.println("입력값이 없습니다.");
         // }
// }
%>
</body>
</html>