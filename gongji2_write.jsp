<!- 실제 글 추가, 수정하는 파일; 웹에 출력되는 부분 없음 ->

<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<%@ page contentType = "text/html; charset=utf-8"%>
<%@ page import="java.sql.*,javax.sql.*, java.io.*"%>
<html>
<head>
<!--에러 잡는 try-catch문-->
<%!      
   public static int extractErrorCode (SQLException sqlException) {
      int errorCode = sqlException.getErrorCode();
      SQLException nested = sqlException.getNextException();
      while(errorCode==0&&nested!=null){
         errorCode=nested.getErrorCode();
         nested=nested.getNextException();
      }
      return errorCode;
   }
%>

<style type = "text/css">
table {
   margin:auto;
   text-align: center;
}   
h2 {
   text-align:center;
}   
</style>
</head>
<body>

<%
try{
	request.setCharacterEncoding("UTF-8");
	//insert.jsp, reinsert.jsp파일에서 파라미터 받아오기
	String id = request.getParameter("id");
	String title = request.getParameter("title");
	String today = request.getParameter("today");
	String content = request.getParameter("content");
	String rootid = request.getParameter("rootid");
	String relevel = request.getParameter("relevel");
	String recnt = request.getParameter("recnt");
	String viewcnt = request.getParameter("viewcnt");
	String key = request.getParameter("key");

	//MySQL드라이버 로딩: class클래스의 forName()메소드를 이용하여 interface driver 로드
	Class.forName("com.mysql.jdbc.Driver");
	
	//MySQL Connection 객체 생성
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/seradatabase", "root","aeae");
	out.println(key);
	//글수정
	if("update".equals(key)){
			
		String sql = "update gongji2 set title=?, content=? where id = ?;";

		PreparedStatement pstmt = conn.prepareStatement(sql);
				
		pstmt.setString(1, title);
		pstmt.setString(2, content); 
		pstmt.setString(3, id);
			
		pstmt.executeUpdate();
		
		pstmt.close();
			
		out.println("id="+id);
		out.println("title="+title);
		out.println("content="+content);
	//신규글 등록
	} else if("insert".equals(key)){
		Statement stmt = conn.createStatement();
		
		String sql = "insert into gongji2(id, title, date, content, rootid, relevel,";
		sql += "recnt) values(?, ?, ?, ?, ?, ?, ?)";
		PreparedStatement pstmt = conn.prepareStatement(sql);
		
		pstmt.setString(1, id);
		pstmt.setString(2, title);
		pstmt.setString(3, today);
		pstmt.setString(4, content); 
		pstmt.setString(5, rootid); //1
		pstmt.setString(6, relevel); //0
		pstmt.setString(7, recnt); //0
		
		
		pstmt.executeUpdate();
		stmt.close();
		pstmt.close();
	//댓글 등록		
	} else if("reinsert".equals(key)) {
		Statement stmt = conn.createStatement();
		stmt.executeUpdate("update gongji2 set recnt=recnt+1 where rootid="+rootid+" and recnt>="+recnt+";");

		String sql = "insert into gongji2(id, title, date, content, rootid, relevel,";
		sql += "recnt) values(?, ?, ?, ?, ?, ?, ?)";
		PreparedStatement pstmt = conn.prepareStatement(sql);
		
		pstmt.setString(1, id);
		pstmt.setString(2, title);
		pstmt.setString(3, today);
		pstmt.setString(4, content); 
		pstmt.setString(5, rootid); //1
		pstmt.setString(6, relevel); //1,2,..
		pstmt.setString(7, recnt); //1,2,..
		
		pstmt.executeUpdate();
		
		stmt.close();
		pstmt.close();
	}

%>

<%
	
	conn.close();
	out.println("<script type=\"text/javascript\"> location.href=\"gongji2_view.jsp?id=" + id + "\"; </script>");
} catch(SQLException e) {
      if(extractErrorCode(e)==1062){
            out.println(extractErrorCode(e)+" 에러가 발생했습니다.<br>");
            out.println("데이터가 중복됩니다.");
         }else if(extractErrorCode(e)==1146){
            out.println(extractErrorCode(e)+" 에러가 발생했습니다.<br>");
            out.println("테이블이 존재하지 않습니다.");
         }else{
            out.println("[기타] <br>");
            out.println(extractErrorCode(e)+" 에러가 발생했습니다.");
            out.println("입력값이 없습니다.");
         }
}
	
%>

</body>
</html>